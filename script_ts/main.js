import { ClockModel, ClockView, ClockController } from "./clock/index.js";
import { TimerModel, TimerView, TimerController, } from "./pomodoro_timer/index.js";
import { TaskModel, TaskView, TaskController } from "./task/index.js";
const mainViewTemplate = (id) => {
    return `
    <!-- Clock -->
    <div class="clock-timer clock-timer-${id}">
    </div>

    <!-- Button Control Pomodoro Timer-->
    <div class="control-btn-timer control-btn-timer-${id}">
    </div>

    <!-- Pomodoro Timer -->
    <div id="pomodoro-timer-clock-${id}">
    </div>

    <!-- Form add task to Table Body below -->
    <div class="form-add-task-${id}">
    </div>

    <!--  Table Tasks -->
    <div class="table-task table-task-${id}">
    </div>
  `;
};
class Pomodoro {
    constructor(id) {
        this.id = id;
        let app = document.querySelector("main");
        let element = document.createElement("div");
        element.id = id;
        app = app.appendChild(element);
        app.innerHTML = mainViewTemplate(this.id);
        this.run();
    }
    run() {
        this.showTime(this.id);
        this.pomodoroTimerClock(this.id);
        this.pomodoroTasks(this.id);
    }
    showTime(id) {
        const clockTimer = new ClockController(new ClockModel(id), new ClockView(id));
    }
    pomodoroTimerClock(id) {
        // const pomodoroTimer1 = new pomodoroTimer(id);
        const pomodoroTimer1 = new TimerController(new TimerModel(), new TimerView(id), id);
    }
    pomodoroTasks(id) {
        // const task1 = new task(id)
        const Task = new TaskController(new TaskModel(id), new TaskView(id), id);
    }
}
const pomodoroApp = new Pomodoro("root");
