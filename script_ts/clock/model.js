export class ClockModel {
    constructor(id) {
        this.id = id;
        this.time = "";
    }
    addTimer() {
        const date = new Date();
        const h = date.getHours();
        const m = date.getMinutes();
        const s = date.getSeconds();
        this.time = `${h}:${m}:${s}`;
        if (s < 10) {
            this.time = `${h}:${m}:0${s}`;
        }
    }
}
