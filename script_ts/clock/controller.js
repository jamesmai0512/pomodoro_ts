export class ClockController {
    constructor(clockModel, clockView) {
        this.clockModel = clockModel;
        this.clockView = clockView;
        this.clockModel.addTimer();
        this.run();
    }
    run() {
        const self = this;
        setInterval(() => {
            self.clockModel.addTimer();
            self.clockView.clockTemplate(self.clockModel.time);
        }, 1000);
    }
}
