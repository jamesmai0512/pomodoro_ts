export class ClockView {
    constructor(id) {
        this.id = id;
        var element = document.createElement("div");
        element.id = `clock-${id}`;
        element.setAttribute("class", "clock clock-display");
        let clockDisplayTimer = document.querySelector(`.clock-timer-${id}`);
        clockDisplayTimer.appendChild(element);
    }
    clockTemplate(time) {
        document.getElementById(`clock-${this.id}`).innerHTML = time;
        document.getElementById(`clock-${this.id}`).textContent = time;
    }
}
