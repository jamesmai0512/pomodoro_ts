import { ClockModel } from "./model.js";
import { ClockView } from "./view.js";
import { ClockController } from "./controller.js";
export { ClockModel, ClockView, ClockController };
