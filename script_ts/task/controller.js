// CONTROLLER
// has given @param Model and View
export class TaskController {
    constructor(model, view, id) {
        // METHODS
        // Check Task Index
        this.taskIndex = (taskId) => this.model.tasks.findIndex((e) => e.id == +taskId);
        // Add New Task
        this.onTaskChange = (tasks) => {
            this.view.addNewTask(tasks, this.id);
        };
        // Update task count
        this.onTaskCount = (taskId) => {
            const taskIndex = this.taskIndex(taskId);
            this.view.updateTaskCount(taskIndex, this.model.tasks);
        };
        // Update task status
        this.onTaskStatusChange = (taskId) => {
            const taskIndex = this.taskIndex(taskId);
            this.view.updateTaskStatus(taskIndex, this.model.tasks);
        };
        // Delete task
        this.onDeleteTask = (taskId) => {
            const taskRow = document.getElementById(`row-${this.id}-${taskId}`);
            taskRow.remove();
        };
        // Binding event for buttons
        // Submit event, calling "addTask" to add data
        this.bindAddTask = () => {
            const pomodoroForm = document.querySelector(`.js-add-task-${this.id}`);
            pomodoroForm.addEventListener("submit", this.handleAddTask);
        };
        // Bind event to add task button
        this.handleAddTask = (e) => {
            e.preventDefault();
            let taskName = document.querySelector(`.js-add-task-name-${this.id}`);
            if (taskName.value) {
                this.model.saveTask(taskName.value);
                this.onTaskChange(this.model.tasks);
                taskName.value = "";
            }
        };
        // Bind event to control button
        this.handleControlButton = () => {
            const pomodoroTableBody = document.querySelector(`.js-task-table-body-${this.id}`);
            pomodoroTableBody.addEventListener("click", this.handleTaskButtonClick);
        };
        // Handling event task button inside table table
        this.handleTaskButtonClick = (event) => {
            const taskTarget = event.target;
            const taskId = taskTarget.dataset.id;
            switch (true) {
                case taskTarget.matches(".js-status-task"):
                    this.model.taskCount(taskId);
                    this.onTaskCount(taskId);
                    break;
                case taskTarget.matches(".js-task-done"):
                    this.model.finishedTask(taskId);
                    this.onTaskStatusChange(taskId);
                    break;
                case taskTarget.matches(".js-delete-task"):
                    this.model.deleteOneById(taskId);
                    this.onDeleteTask(taskId);
                    break;
            }
        };
        this.model = model;
        this.view = view;
        this.id = id;
        this.view.render();
        this.bindAddTask();
        this.handleControlButton();
        this.view.renderList(this.model.tasks, this.id);
    }
}
// END CONTROLLER
