import { tableTaskTemplate, addTaskTemplate, updateTaskStatusTemplate, } from "./template.js";
// VIEW
export class TaskView {
    constructor(id) {
        this.id = id;
    }
    template(id) {
        return `
        <form class="js-add-task-${id}" action="">
          <fieldset class="add-task-fieldset">
            <input class="js-add-task-name js-add-task-name-${id}" type="text" placeholder="Task Name" />
            <button class="btn add-task-btn" type="submit">Add</button>
          </fieldset>
        </form>
      `;
    }
    render() {
        // Form Add Task
        // Form Add Tasks
        const formAddTask = document.querySelector(`.form-add-task-${this.id}`);
        formAddTask.innerHTML = this.template(this.id);
        // Add Table Body
        const tableTask = document.querySelector(`.table-task-${this.id}`);
        tableTask.innerHTML = tableTaskTemplate(this.id);
    }
    // Add tasks at first
    renderList(list, id) {
        this.id = id;
        const tableBodyNode = document.querySelector(`.js-task-table-body-${this.id}`);
        tableBodyNode.innerHTML = list
            .map((task) => addTaskTemplate(task, this.id))
            .join("");
    }
    // Adding new task
    addNewTask(tasks, id) {
        this.id = id;
        const tableBodyNode = document.querySelector(`.js-task-table-body-${this.id}`);
        const task = tasks[tasks.length - 1];
        tableBodyNode.innerHTML += addTaskTemplate(task, this.id);
    }
    // Change the task status finished
    updateTaskStatus(taskIndex, tasks) {
        const task = tasks[taskIndex];
        const buttonRow = document.querySelector(`.cell-pom-controls-${this.id}-${task.id}`);
        buttonRow.innerHTML = updateTaskStatusTemplate(task);
    }
    // Change The task count
    updateTaskCount(taskIndex, tasks) {
        const task = tasks[taskIndex];
        const pomCount = document.querySelector(`.cell-pom-count-${this.id}-${task.id}`);
        pomCount.innerHTML = `${task.pomodoroDone} Pomodori`;
    }
}
// END VIEW
