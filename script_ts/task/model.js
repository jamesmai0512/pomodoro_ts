import { DBService } from "../service/db.js";
// MODEL
export class TaskModel {
    constructor(id) {
        this.id = id;
        this.newDBService = new DBService("development");
        this.tasks = JSON.parse(this.newDBService.get(`tasks-${this.id}`)) || [];
        // JSON.parse() to convert text into a JavaScript object:
    }
    // Methods of Task Model
    saveTaskData(tasks) {
        this.newDBService.save(`tasks-${this.id}`, JSON.stringify(tasks)); // Convert a JavaScript object into a string with JSON.stringify().
    }
    // Save Tasks
    saveTask(taskName) {
        if (taskName) {
            this.tasks.push({
                id: this.tasks.length > 0 ? this.tasks[this.tasks.length - 1].id + 1 : 1,
                taskName,
                pomodoroDone: 0,
                finished: false,
            });
            this.save(this.tasks);
        }
        else {
            return false;
        }
    }
    save(data) {
        this.saveTaskData(data);
        return true;
    }
    // Count the Task
    taskCount(id) {
        const taskIndex = this.tasks.findIndex((e) => e.id == +id);
        this.tasks[taskIndex].pomodoroDone += 1;
        this.update(this.tasks);
    }
    // Task user has finished
    finishedTask(id) {
        const taskIndex = this.tasks.findIndex((e) => e.id == +id);
        this.tasks[taskIndex].finished = true;
        this.update(this.tasks);
    }
    update(data) {
        if (data) {
            this.saveTaskData(data);
            return true;
        }
        else {
            return false;
        }
    }
    // Delete the task following ID
    deleteOneById(id) {
        if (this.tasks) {
            this.saveTaskData(this.tasks.filter((task) => task.id != +id));
            return true;
        }
        else {
            return false;
        }
    }
}
// END MODEL
