// Template Helper
const formAddTaskTemplate = (id) => {
    return `
    <form class="js-add-task-${id}" action="">
      <fieldset class="add-task-fieldset">
        <input class="js-add-task-name js-add-task-name-${id}" type="text" placeholder="Task Name" />
        <button class="btn add-task-btn" type="submit">Add</button>
      </fieldset>
    </form>
  `;
};
const tableTaskTemplate = (id) => {
    return `
    <table>
      <thead>
        <tr>
          <th>Task Name</th>
          <th>Status</th>
          <th>Controls</th>
        </tr>
      </thead>

      <!-- Table body  -->
      <tbody class="js-task-table-body-${id}">
        
      </tbody>
    </table>
  `;
};
const addTaskTemplate = (task, id) => {
    return `
    <tr id="row-${id}-${task.id}">
      <td class="cell-task-name">${task.taskName}</td>
      <td class="cell-pom-count cell-pom-count-${id}-${task.id}">${task.pomodoroDone} Pomodori</td>
      <td class="cell-pom-controls cell-pom-controls-${id}-${task.id}">
        ${task.finished
        ? "Finished"
        : `
          <button class="js-status-task control-task-btn" data-id="${task.id}">Increase Pomodoro Count</button>
          <button class="js-task-done control-task-btn" data-id="${task.id}">Done</button>`}
        <button class="js-delete-task control-task-btn" data-id="${task.id}">Delete</button>
      </td>
    </tr>
  `;
};
const updateTaskStatusTemplate = (task) => {
    return `
    ${task.finished
        ? "Finished"
        : `
      <button class="js-status-task control-task-btn" data-id="${task.id}">Increase Pomodoro Count</button>
      <button class="js-task-done control-task-btn" data-id="${task.id}">Done</button>`}
    <button class="js-delete-task control-task-btn" data-id="${task.id}">Delete</button>
  `;
};
export { formAddTaskTemplate, tableTaskTemplate, addTaskTemplate, updateTaskStatusTemplate, };
