const localStorageService = {
    // Handle the task data
    save: (resource, task) => localStorage.setItem(resource, task),
    get: (resource) => localStorage.getItem(resource),
};
const restAPIService = {};
export class DBService {
    constructor(env) {
        this.save = (resource, item) => {
            const dbService = this.env == "development" ? localStorageService : restAPIService;
            dbService.save(resource, item);
        };
        this.get = (resource) => {
            const dbService = this.env == "development" ? localStorageService : restAPIService;
            return dbService.get(resource);
        };
        this.env = env;
    }
}
