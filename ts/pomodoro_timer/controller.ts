import { TimerModel as model } from "./model";
import { TimerView as view } from "./view";
interface ITimerController {
  id: string;
  model: model;
  view: view;

  bindEventStartButton: () => void;
  addTimer: () => void;
  startTimer: () => void;
}
export class TimerController implements ITimerController {
  id: string;
  model: model;
  view: view;
  constructor(model: model, view: view, id: string) {
    this.model = model;
    this.view = view;
    this.id = id;

    this.view.pomodoroTimer();
    this.view.renderControlsButton();
    this.view.addTimerTemplate(
      this.model.session_minutes,
      this.model.session_seconds
    );
    this.bindEventStartButton();
  }
  bindEventStartButton() {
    const element = document.querySelector(
      `.start-btn-${this.id}`
    )! as HTMLElement;
    element.onclick = () => {
      this.startTimer();
    };
  }
  addTimer() {
    this.view.addMinuteTimer(this.model.session_minutes);
    this.view.addSecondTimer(this.model.session_seconds);
  }
  startTimer() {
    const element = document.querySelector(
      `.start-btn-${this.id}`
    )! as HTMLElement;
    element.onclick = () => {};

    this.model.timerSession();
    this.addTimer();
    // Call "minutesTimer" every one min
    var minute_interval = setInterval(() => {
      this.model.sessionMinutes();
      this.view.addMinuteTimer(this.model.session_minutes);
    }, 60000);

    var second_interval = setInterval(() => {
      this.model.sessionSeconds();
      console.log(this.model.session_seconds);
      this.view.addSecondTimer(this.model.session_seconds);
      // secondsTimer(this.model.session_seconds, this.model.session_minutes);
      if (+this.model.session_seconds <= 0) {
        if (+this.model.session_minutes <= 0) {
          clearInterval(second_interval);
          clearInterval(minute_interval);
          this.model.resetTimer();
          this.addTimer();
          const element = document.querySelector(
            `.start-btn-${this.id}`
          )! as HTMLElement;
          element.onclick = () => {
            this.startTimer();
          };
        }
        this.model.resetSecond();
      }
    }, 1000);
  }
}
