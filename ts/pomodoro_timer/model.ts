interface ITimerModel {
  session_seconds: string;
  seconds_number: number;
  session_minutes: string;
  minutes_number: number;

  timerSession: () => void;
  sessionSeconds: () => void;
  sessionMinutes: () => void;
  resetSecond: () => void;
  resetTimer: () => void;
}
export class TimerModel implements ITimerModel {
  session_seconds: string;
  seconds_number: number;
  session_minutes: string;
  minutes_number: number;

  constructor() {
    this.session_seconds = "00";
    this.seconds_number = 0;
    this.session_minutes = "25";
    this.minutes_number = 25;
  }

  timerSession() {
    this.session_seconds = "59";
    this.session_minutes = "24";
  }
  sessionSeconds() {
    this.seconds_number = +this.session_seconds - 1;
    this.session_seconds =
      this.seconds_number < 10
        ? "0" + `${this.seconds_number}`
        : `${this.seconds_number}`;
  }
  sessionMinutes() {
    this.minutes_number = +this.session_minutes - 1;
    this.session_minutes =
      this.minutes_number < 10
        ? "0" + `${this.minutes_number}`
        : `${this.minutes_number}`;
  }
  resetSecond() {
    this.session_seconds = "60";
  }
  resetTimer() {
    this.session_minutes = "25";
    this.session_seconds = "00";
  }
}
