import { TaskModel as model } from "./model";
import { TaskView as view } from "./view";
import { ITask } from "./index";

interface ITaskController {
  id: string;
  model: model;
  view: view;

  taskIndex: (taskId: string) => void;
  onTaskChange: (tasks: [ITask]) => void;
  onTaskCount: (taskId: string) => void;
  onTaskStatusChange: (taskId: string) => void;
  onDeleteTask: (taskId: string) => void;
  bindAddTask: () => void;
  handleAddTask: (e: Event) => void;
  handleControlButton: () => void;
  handleTaskButtonClick: (event: Event) => void;
}

// CONTROLLER
// has given @param Model and View
export class TaskController implements ITaskController {
  id: string;
  model: model;
  view: view;

  constructor(model: model, view: view, id: string) {
    this.model = model;
    this.view = view;
    this.id = id;

    this.view.render();
    this.bindAddTask();
    this.handleControlButton();

    this.view.renderList(this.model.tasks, this.id);
  }
  // METHODS
  // Check Task Index
  taskIndex = (taskId: string) =>
    this.model.tasks.findIndex((e) => e.id == +taskId);
  // Add New Task
  onTaskChange = (tasks: [ITask]) => {
    this.view.addNewTask(tasks, this.id);
  };
  // Update task count
  onTaskCount = (taskId: string) => {
    const taskIndex = this.taskIndex(taskId);
    this.view.updateTaskCount(taskIndex, this.model.tasks);
  };
  // Update task status
  onTaskStatusChange = (taskId: string) => {
    const taskIndex = this.taskIndex(taskId);
    this.view.updateTaskStatus(taskIndex, this.model.tasks);
  };
  // Delete task
  onDeleteTask = (taskId: string) => {
    const taskRow = document.getElementById(`row-${this.id}-${taskId}`)!;
    taskRow.remove();
  };
  // Binding event for buttons
  // Submit event, calling "addTask" to add data
  bindAddTask = () => {
    const pomodoroForm = document.querySelector(`.js-add-task-${this.id}`)!;
    pomodoroForm.addEventListener("submit", this.handleAddTask);
  };
  // Bind event to add task button
  handleAddTask = (e: Event) => {
    e.preventDefault();
    let taskName = document.querySelector(
      `.js-add-task-name-${this.id}`
    )! as HTMLInputElement;

    if (taskName.value) {
      this.model.saveTask(taskName.value);
      this.onTaskChange(this.model.tasks);
      taskName.value = "";
    }
  };
  // Bind event to control button
  handleControlButton = () => {
    const pomodoroTableBody = document.querySelector(
      `.js-task-table-body-${this.id}`
    )! as HTMLElement;
    pomodoroTableBody.addEventListener("click", this.handleTaskButtonClick);
  };
  // Handling event task button inside table table
  handleTaskButtonClick = (event: Event) => {
    const taskTarget = event.target! as HTMLElement;
    const taskId = taskTarget.dataset.id!;

    switch (true) {
      case taskTarget.matches(".js-status-task"):
        this.model.taskCount(taskId);
        this.onTaskCount(taskId);
        break;
      case taskTarget.matches(".js-task-done"):
        this.model.finishedTask(taskId);
        this.onTaskStatusChange(taskId);
        break;
      case taskTarget.matches(".js-delete-task"):
        this.model.deleteOneById(taskId);
        this.onDeleteTask(taskId);
        break;
    }
  };
}
// END CONTROLLER
