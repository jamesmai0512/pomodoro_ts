import { DBService } from "../service/db.js";
import { IModel, IListModel } from "../base/index";
import { ITask } from "./index";

interface ITaskModel {
  id: string;
  tasks: [task: ITask];
  newDBService: DBService;

  saveTaskData: (tasks: [ITask]) => void;
  saveTask: (taskName: string) => void;
  taskCount: (id: string) => void;
  finishedTask: (id: string) => void;
}

// MODEL
export class TaskModel implements ITaskModel, IModel<ITask>, IListModel<ITask> {
  id: string;
  tasks: [task: ITask];
  newDBService: DBService;
  constructor(id: string) {
    this.id = id;
    this.newDBService = new DBService("development");
    this.tasks = JSON.parse(this.newDBService.get(`tasks-${this.id}`)) || [];
    // JSON.parse() to convert text into a JavaScript object:
  }

  // Methods of Task Model
  saveTaskData(tasks: Partial<ITask>[]) {
    this.newDBService.save(`tasks-${this.id}`, JSON.stringify(tasks)); // Convert a JavaScript object into a string with JSON.stringify().
  }

  // Save Tasks
  saveTask(taskName: string) {
    if (taskName) {
      this.tasks.push({
        id:
          this.tasks.length > 0 ? this.tasks[this.tasks.length - 1].id + 1 : 1,
        taskName,
        pomodoroDone: 0,
        finished: false,
      });
      this.save(this.tasks);
    } else {
      return false;
    }
  }
  save(data: Partial<ITask>[]) {
    this.saveTaskData(data);
    return true;
  }
  // Count the Task
  taskCount(id: string) {
    const taskIndex = this.tasks.findIndex((e) => e.id == +id);
    this.tasks[taskIndex].pomodoroDone += 1;
    this.update(this.tasks);
  }
  // Task user has finished
  finishedTask(id: string) {
    const taskIndex: number = this.tasks.findIndex((e) => e.id == +id);
    this.tasks[taskIndex].finished = true;
    this.update(this.tasks);
  }

  update(data: Partial<ITask>[]) {
    if (data) {
      this.saveTaskData(data);
      return true;
    } else {
      return false;
    }
  }
  // Delete the task following ID
  deleteOneById(id: string) {
    if (this.tasks) {
      this.saveTaskData(this.tasks.filter((task: ITask) => task.id != +id));
      return true;
    } else {
      return false;
    }
  }
}
// END MODEL
