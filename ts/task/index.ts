import { TaskModel } from "./model.js";
import { TaskView } from "./view.js";
import { TaskController } from "./controller.js";

interface ITask {
  id: number;
  taskName: string;
  pomodoroDone: number;
  finished: boolean;
}

export { TaskModel, TaskView, TaskController, ITask };
