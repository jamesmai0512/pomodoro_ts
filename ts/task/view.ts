import { IView, IListView } from "../base/index";
import { ITask } from "./index";
import {
  formAddTaskTemplate,
  tableTaskTemplate,
  addTaskTemplate,
  updateTaskStatusTemplate,
} from "./template.js";

interface ITaskView {
  id: string;

  addNewTask: (tasks: [ITask], id: string) => void;
  updateTaskStatus: (taskIndex: number, tasks: [ITask]) => void;
  updateTaskCount: (taskIndex: number, tasks: [ITask]) => void;
}
// VIEW
export class TaskView implements ITaskView, IView<ITask>, IListView<ITask> {
  id: string;
  constructor(id: string) {
    this.id = id;
  }

  template(id: string) {
    return `
        <form class="js-add-task-${id}" action="">
          <fieldset class="add-task-fieldset">
            <input class="js-add-task-name js-add-task-name-${id}" type="text" placeholder="Task Name" />
            <button class="btn add-task-btn" type="submit">Add</button>
          </fieldset>
        </form>
      `;
  }

  render() {
    // Form Add Task
    // Form Add Tasks
    const formAddTask = document.querySelector(
      `.form-add-task-${this.id}`
    )! as HTMLElement;
    formAddTask.innerHTML = this.template(this.id);
    // Add Table Body
    const tableTask = document.querySelector(
      `.table-task-${this.id}`
    )! as HTMLElement;
    tableTask.innerHTML = tableTaskTemplate(this.id);
  }

  // Add tasks at first
  renderList(list: ITask[], id: string) {
    this.id = id;
    const tableBodyNode = document.querySelector(
      `.js-task-table-body-${this.id}`
    )! as HTMLElement;
    tableBodyNode.innerHTML = list
      .map((task) => addTaskTemplate(task, this.id))
      .join("");
  }

  // Adding new task
  addNewTask(tasks: [ITask], id: string) {
    this.id = id;
    const tableBodyNode = document.querySelector(
      `.js-task-table-body-${this.id}`
    )! as HTMLElement;
    const task = tasks[tasks.length - 1];
    tableBodyNode.innerHTML += addTaskTemplate(task, this.id);
  }

  // Change the task status finished
  updateTaskStatus(taskIndex: number, tasks: [ITask]) {
    const task = tasks[taskIndex];

    const buttonRow = document.querySelector(
      `.cell-pom-controls-${this.id}-${task.id}`
    )! as HTMLElement;
    buttonRow.innerHTML = updateTaskStatusTemplate(task);
  }
  // Change The task count
  updateTaskCount(taskIndex: number, tasks: [ITask]) {
    const task = tasks[taskIndex];
    const pomCount = document.querySelector(
      `.cell-pom-count-${this.id}-${task.id}`
    )! as HTMLElement;
    pomCount.innerHTML = `${task.pomodoroDone} Pomodori`;
  }
}
// END VIEW
