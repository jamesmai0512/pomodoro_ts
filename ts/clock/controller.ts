import { ClockModel } from "model";
import { ClockView } from "view";
interface IClockController {
  clockModel: ClockModel;
  clockView: ClockView;
}
export class ClockController implements IClockController {
  clockModel: ClockModel;
  clockView: ClockView;

  constructor(clockModel: ClockModel, clockView: ClockView) {
    this.clockModel = clockModel;
    this.clockView = clockView;
    this.clockModel.addTimer();
    this.run()
  }
  run() {
    const self = this;
    setInterval(() => {
      self.clockModel.addTimer();
      self.clockView.clockTemplate(self.clockModel.time);
    }, 1000);
  }
}
