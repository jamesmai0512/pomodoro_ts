interface IClockModel {
  id: string;
  time: string;

  addTimer: () => void;
}

export class ClockModel implements IClockModel {
  id: string;
  time: string;

  constructor(id: string) {
    this.id = id;
    this.time = "";
  }

  addTimer() {
    const date: Date = new Date();
    const h: number = date.getHours();
    const m: number = date.getMinutes();
    const s: number = date.getSeconds();

    this.time = `${h}:${m}:${s}`;
    if (s < 10) {
      this.time = `${h}:${m}:0${s}`;
    }
  }
}
