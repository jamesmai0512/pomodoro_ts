interface IClockView {
  id: string;
  clockTemplate: (time: string) => void;
}

export class ClockView implements IClockView {
  id: string;

  constructor(id: string) {
    this.id = id;
    var element = document.createElement("div");
    element.id = `clock-${id}`;
    element.setAttribute("class", "clock clock-display");
    let clockDisplayTimer = document.querySelector(`.clock-timer-${id}`)!;
    clockDisplayTimer.appendChild(element);
  }

  clockTemplate(time: string) {
    document.getElementById(`clock-${this.id}`)!.innerHTML = time;
    document.getElementById(`clock-${this.id}`)!.textContent = time;
  }
}
