export interface IModel<T> {
  id: string;
  save?: (data: Partial<T>[]) => Boolean;
  update?: (data: Partial<T>[]) => Boolean;
  delete?: () => Boolean;
  get?: (id?: String) => T;
}

export interface IListModel<T> {
  fetch?: () => T[];
  findById?: (id: string) => T;
  deleteOneById?: (id: string) => Boolean;
  updateOne?: (id: String) => Boolean;
  addOne?: (item: T) => Boolean;
}
