import { IModel, IListModel } from "../base/model";
import { IView, IListView } from "../base/view";

export { IModel, IListModel, IView, IListView };
