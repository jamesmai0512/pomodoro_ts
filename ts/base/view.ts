interface IView<T> {
  id: string;
  template?: (id: string) => void;
  render?: () => void;
  queryOne?: () => HTMLElement;
  queryMany?: () => HTMLElement;
}

interface IListView<T> {
  id: string;
  renderList?: (list: T[], id: string) => void;
}

export { IView, IListView };
